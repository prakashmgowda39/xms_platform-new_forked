import React, {Component} from 'react';
import "./NewTaskListing.scss";
import MainLayout from '../../containers/MainLayout/MainLayout';
import NewTaskCollapsable from '../../containers/NewTaskListing/NewTaskCollapsable/NewTaskCollapsable';
import Header from "../../containers/Header/Header";
import SubHeader from "../../containers/SubHeaderNew/SubHeaderNew";

class NewTasksListing extends Component{
    constructor(props){
        super(props);
        this.state ={

        }
    }
    bodySection = () =>{
        return(
            <div className='tsks-lstng-body-section'>
                <Header />
                <SubHeader />
                <div className="tsks-lstng-body-container">

                </div>
            </div>
        )
    }
    render() {
        return <MainLayout secondSidebar={<NewTaskCollapsable thisObj={this} refreshData={this.refreshData} client={this.props.client} />} bodySection={this.bodySection()}  />
      }
}
export default NewTasksListing;