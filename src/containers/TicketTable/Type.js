import React, {Component} from "react";
import "./Type.css";

class Type extends Component{
    constructor(props){
        super(props)
        this.invokeParentMethod = this.invokeParentMethod.bind(this);
    }
    invokeParentMethod() {
        this.props.context.componentParent.methodFromParent(`Row: ${this.props.node.rowIndex}, Col: ${this.props.colDef.headerName}`)
    }
    render(){
        return(
            <p className={`ticket-type ${this.props.data.ticketType === "Service Request" ? "service-request" : this.props.data.ticketType === "Incident" ? "incident" :this.props.data.ticketType === "Problem" ? "problem" : null } `}>{this.props.data.ticketType}</p>
        )
    }
    
}
export default Type;