import React from 'react';
import "./MergeTaskDrawer.scss";
import Drawer from '@material-ui/core/Drawer';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import Button from '@material-ui/core/Button';
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import Checkbox from '@material-ui/core/Checkbox';
// import AssignToImage from "../../../../assets/images/profile.png";
import AssignToImage from "../../../../assets/images/profile.png";
// import PrimaryIcon from "../../../../../assets/icons/01-10-2019/Icon-awesome-star-of-life.svg";
import PrimaryIcon from "../../../../assets/icons/01-10-2019/Icon-awesome-star-of-life.svg";

import SubtractIcon from "../../../../assets/icons/18-11-2019/Subtraction 7.svg";
import PrimaryIconBlue from "../../../../assets/icons/18-11-2019/PrimaryIconBlue.svg";
import PrimaryIconGray from "../../../../assets/icons/18-11-2019/PrimaryIconGray.svg";


import axios from "axios";
import { baseUrl } from "../../../../constants";

const names = [
    'Oliver Hansen',
    'Van Henry',
    'April Tucker',
    'Ralph Hubbard',
    'Omar Alexander',
    'Carlos Abbott',
    'Miriam Wagner',
    'Bradley Wilkerson',
    'Virginia Andrews',
    'Kelly Snyder',
];

let checkedList = [];
class MergeTicket extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            category: "",
            searchText: "",
            responseData: "",
            checkBoxValues: [],
            primarySelect:this.props.selectedMergeTask.id,
        }
    }

    selectPrimary=(data)=>{
        this.setState({primarySelect:data})
    }

    handleChange = event => {
        this.setState({ priority: event.target.value });
    };
    setCategory = (event) => {
        this.setState({ category: event.target.value });
    }

    handleSearchText = event => {
        this.setState({ searchText: event.target.value });
    };

    handleCheckBoxValue = (e) => {
        if (e.target.checked) {
            checkedList.push(Number(e.target.value));
        } else if (!e.target.checked) {
            checkedList.pop();
        }
    }
    removeTaskMerge=(data)=>{
        let array = this.props.selectedTask
        let index = array.indexOf(data);
        array.splice(index,1);
        this.props.thisObj.setState({selectedTask:array})
    }

    submitSearchText = async (e) => {
        let requestBody = {
            query: `
              mutation searchTicketBasedOnCategory(
                  $searchText: String,
                  $category: String,
                ) {
                searchTicketBasedOnCategory(
                    category: $category,
                    categoryKeyWord:$searchText
                ) {
                    id
                    name,
                    ticketDescription
                }
              }
            `,
            variables: {
                searchText: this.state.searchText &&
                    this.state.searchText != '' ?
                    this.state.searchText : '',
                category: this.state.category
            }
        };

        let resData = await axios({
            method: 'post',
            url: baseUrl.server,
            data: requestBody,
        }).then(res => {
            return res
        }).catch(err => {
            this.setState({ responseData: '' });
            return err;
        });
        this.setState({
            responseData: resData.data != null ?
                resData.data.data.searchTicketBasedOnCategory : ''
        });
        console.log("SEARCH RESPONSE", resData);
    }



    closeMDrawerHandler = () => {
        this.props.thisObj.setState({ isMergeDrawerOpen: false })
    }
    chooseStartHandler = date => {
        this.setState({ startDate: date })
    }

    chooseDueHandler = date => {
        this.setState({ dueDate: date })
    }
    toggleDrawer = (side, open) => event => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }

        this.setState({ ...this.state, [side]: open });
    };



    priorityHandleChange = event => {
        console.log('value', event.target.value)
        this.setState({ priority: event.target.value })
    };

    multipleHandleChange = event => {
        this.setState({ tagsName: event.target.value });
    };

    sideList = side => {
       return <div className={'merge-ticket-drawer-bodySection'} role="presentation"  >
            <div className="merge-ticket">
                <div className="merge-ticket-header-text"><p>MERGE</p></div>
                <div className="merge-ticket-drop-down-search">
                    <FormControl>
                        <InputLabel htmlFor="demo-controlled-open-select">Task</InputLabel>
                        <div className="merge-ticket-menu-item">
                            <Select
                                open={this.state.open}
                                onClose={this.handleOpenClose}
                                onOpen={this.handleOpenClose}
                                value={this.state.category}
                                onChange={this.setCategory}
                            >
                                <MenuItem value="ticketNo">Task Id</MenuItem>
                                <MenuItem value="status">Status</MenuItem>
                                <MenuItem value="tags">Tag</MenuItem>
                                <MenuItem value="priority">Priority</MenuItem>
                            </Select>
                        </div>
                    </FormControl>

                    <div className="header-search">
                        <div className="search-bar d-flex">

                            <InputBase
                                placeholder="Search Tasks"
                                className="search-input"
                                inputProps={{ 'aria-label': 'search' }}
                                onChange={this.handleSearchText}
                                onKeyPress={this.submitSearchText}
                            />

                            <div className="search-icon">
                                <SearchIcon />
                            </div>
                        </div>
                    </div>

                </div>

                <div className="merge-ticket-licensing-container">
{this.props.selectedTask !=null || this.props.selectedTask != undefined ? 
    this.props.selectedTask.map(task=> {
        if(this.state.primarySelect == task.id){
                return <div className={this.state.primarySelect == task.id ? "merge-ticket-licensing" : "merge-ticket-licensing-one"}>
                        <Checkbox
                            value="checkedB"
                            color="primary"
                        />
                        <div className="merge-ticket-phone-number-text">
                            <p className="merge-ticket-phone-number-text-one">#{task.id}</p>
                            <p className="merge-ticket-phone-number-text-two">Code Matrix</p>
                        </div>
                        <div className="merge-ticket-licensing-text">
                            <p>{task.taskTitle}</p>
                            <div className="merge-ticket-licensing-img-text">
                                <img src={AssignToImage}></img>
                                <p>Sugatha Maji</p>
                            </div>
                        </div>


                        {/* <div onClick={()=>this.selectPrimary(task.id)} className={this.state.primarySelect == task.id ?"merge-ticket-primary":"merge-ticket-primary-one"}> */}
                            {/* <img src={PrimaryIconBlue}></img> */}
                            <img onClick={()=>this.selectPrimary(task.id)} src={PrimaryIconBlue}></img>

                            {/* <p>PRIMARY</p> */}
                        {/* </div> */}
                    </div>
                    {/* <div className="merge-ticket-licensing-one">
                        <Checkbox
                            value="checkedB"
                            color="primary"
                        />
                        <div className="merge-ticket-phone-number-text">
                            <p className="merge-ticket-phone-number-text-one">#0828190</p>
                            <p className="merge-ticket-phone-number-text-two">Code Matrix</p>
                        </div>
                        <div className="merge-ticket-licensing-text">
                            <p>Need one more licensing</p>
                            <div className="merge-ticket-licensing-img-text">
                                <img src={AssignToImage}></img>
                                <p>Sugatha Maji</p>
                            </div>
                        </div>
                        <div className="merge-ticket-primary-one">
                            <img src={PrimaryIcon}></img>
                            <p>PRIMARY</p>
                        </div>
                    </div> */}
                    {/* <div className="merge-ticket-licensing-one">
                        <Checkbox
                            value="checkedB"
                            color="primary"
                        />
                        <div className="merge-ticket-phone-number-text">
                            <p className="merge-ticket-phone-number-text-one">#0828190</p>
                            <p className="merge-ticket-phone-number-text-two">Code Matrix</p>
                        </div>
                        <div className="merge-ticket-licensing-text">
                            <p>Need one more licensing</p>
                            <div className="merge-ticket-licensing-img-text">
                                <img src={AssignToImage}></img>
                                <p>Sugatha Maji</p>
                            </div>
                        </div>
                        <div className="merge-ticket-primary-two">
                            <img src={PrimaryIcon}></img>
                            <p>PRIMARY</p>
                        </div>
                    </div> */}
                }
                })
                : null
}
{this.props.selectedTask !=null || this.props.selectedTask != undefined ? 
    this.props.selectedTask.map(task=> {
        if(this.state.primarySelect != task.id){
                return <div className={this.state.primarySelect == task.id ? "merge-ticket-licensing" : "merge-ticket-licensing-one"}>
                        {/* <Checkbox
                            value="checkedB"
                            color="primary"
                            onClick={()=>this.removeTaskMerge(task)}
                        /> */}
                        <img src={SubtractIcon} onClick={()=>this.removeTaskMerge(task)}></img>
                        <div className="merge-ticket-phone-number-text">
                            <p className="merge-ticket-phone-number-text-one">#{task.id}</p>
                            <p className="merge-ticket-phone-number-text-two">Code Matrix</p>
                        </div>
                        <div className="merge-ticket-licensing-text">
                            <p>{task.taskTitle}</p>
                            <div className="merge-ticket-licensing-img-text">
                                <img src={AssignToImage}></img>
                                <p>Sugatha Maji</p>
                            </div>
                        </div>


                        {/* <div  className={this.state.primarySelect == task.id ?"merge-ticket-primary":"merge-ticket-primary-one"}> */}
                            <img onClick={()=>this.selectPrimary(task.id)} src={PrimaryIconGray}></img>
                            {/* <p>PRIMARY</p> */}
                        {/* </div> */}
                    </div>
    }  
            })
                : null
}
                </div>


                

<div className="merge-break-for-submit-section"></div>
                <div className="bulk-update-submit-section">
                    <div className="bulk-update-submit-section-button-one">
                        <Button variant="outlined" color="primary" className="" onClick={this.closeMDrawerHandler}>
                            Cancel
                        </Button>
                    </div>
                    <div className="bulk-update-submit-section-button-two">
                        <Button variant="outlined" color="primary" className="">
                            Update
                        </Button>
                    </div>

                </div>


            </div>

        </div>
    };

    render() {
        return (
            <Drawer anchor="right" open={this.props.isMergeDrawerOpen} onClose={this.toggleDrawer('right', false)} className='merge-ticket-drawer'>
                {this.sideList('right')}
            </Drawer>
        )
    }

}

export default MergeTicket;
