import React from 'react';
// import '../FiltersDrawer/FiltersDrawer.scss';
import './CreateTicketDrawer.scss';
import Drawer from '@material-ui/core/Drawer';
import CreateTicketDrawerClose from '../../../../../assets/icons/01-10-2019/Icon ionic-ios-close.svg';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import Input from '@material-ui/core/Input';
import Button from '@material-ui/core/Button';
import AttachmentAddIcon from "../../../../../assets/icons/01-10-2019/Icon feather-plus-circle.svg";
import CompanyLogo from "../../../../../assets/icons/create-ticket/Group 10506.svg";
import CompanyEditIcon from "../../../../../assets/icons/SVG/Iconfeather-edit-3.svg";
import RequestorProfile from "../../../../../assets/images/profile.png";
import RequestorDropDown from "../../../../../assets/icons/LoginAndRegistration_icons/Icon-ionic-md-arrow-dropdown.svg";
import KnowledgeIcon from "../../../../../assets/icons/create-ticket/Icon material-content-paste.svg";
import Checkbox from '@material-ui/core/Checkbox';
import { withStyles } from '@material-ui/core/styles';
import Menu from '@material-ui/core/Menu';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import DraftsIcon from '@material-ui/icons/Drafts';
import SendIcon from '@material-ui/icons/Send';
import DropDwnIcon from "../../../../../assets/icons/SVG/Icon ionic-ios-arrow-down.svg";
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import QckCreateChck from "../../../../../assets/icons/01-10-2019/Icon feather-check-circle.svg";
import { Scrollbars } from 'react-custom-scrollbars';
import profile from "../../../../../assets/images/profile.png";
import AddMultipleTag from "../../../../../assets/icons/01-10-2019/Icon feather-plus-circle.svg"

import axios from "axios";
import { baseUrl } from "../../../../../constants";
// import tagsIcon from '../../assets/create-habit/Icon awesome-tags.svg'
import EditCompanyDetails from '../../../../../components/TicketingSystem/CreateTicket/EditCompanyDetails/EditCompanyDetails';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Radio from '@material-ui/core/Radio';
import WebAppIcon from "../../../../../assets/icons/create-ticket/globe-asia.svg";
import ChatIcon from "../../../../../assets/icons/create-ticket/chat_bubble_outline.svg";
import PhoneIcon from "../../../../../assets/icons/create-ticket/phone-alt.svg";
import EmailIcon from "../../../../../assets/icons/create-ticket/email.svg";
import SMSIcon from "../../../../../assets/icons/create-ticket/chat_bubble_outline.svg";
import MobileIcon from "../../../../../assets/icons/create-ticket/mobile-app.svg";
import CriticalIcon from "../../../../../assets/icons/create-ticket/Rectangle 643.svg";
import HighIcon from "../../../../../assets/icons/create-ticket/Rectangle 242.svg";
import MediumIcon from "../../../../../assets/icons/create-ticket/Path 887.svg";
import LowIcon from "../../../../../assets/icons/create-ticket/Rectangle 261.svg";
import CreateEmailIcon from "../../../../../assets/icons/create-ticket/Icon feather-mail.svg";
import DrpDwnIcn from "../../../../../assets/icons/create-ticket/Icon ionic-md-arrow-dropdown.svg";
import KnowledgeSearch from "../../../../../assets/icons/create-ticket/Icon feather-search.svg";
import CriticalCustomerIcon from "../../../../../assets/icons/create-ticket/Ellipse 3149.svg";
import SearchTagAdd from "../../../../../assets/icons/create-ticket/Group 11349.svg";
import { CirclePicker, TwitterPicker } from 'react-color';
import ClrPckrTray from "../../../../../assets/icons/create-ticket/Icon ionic-ios-color-palette.svg";
import { getTicketTemplatesData, getUserEmails, getAllPriority, getAllTicketTypes, getAllTags } from "./createTicketQueries";
import { InMemoryCache } from 'apollo-cache-inmemory';
import ApolloClient from "apollo-boost";

const cache = new InMemoryCache();

const client = new ApolloClient({
  cache,
  uri: baseUrl.server,
});


const teamTabPanel = []

const peopleTabPanel = []

const QuickCreateIcons = []

const Tags = [
  // { id: 1, icon: MediumIcon, title: "Risk" },
  // { id: 2, icon: CriticalCustomerIcon, title: "Critical Customer" },
  // { id: 3, icon: HighIcon, title: "Phase1" },
  // { id: 4, icon: LowIcon, title: "Technical" },
  // { id: 5, icon: MediumIcon, title: "Risk" },
]


const Source = [
  { id: 1, icon: WebAppIcon, title: "Web App" },
  { id: 2, icon: ChatIcon, title: "Chat" },
  { id: 3, icon: PhoneIcon, title: "Phone" },
  { id: 4, icon: EmailIcon, title: "Email" },
  { id: 5, icon: SMSIcon, title: "SMS" },
  { id: 6, icon: MobileIcon, title: "Mobile App" },

]

const Email = []

const Priority = [
  { id: 1, icon: CriticalIcon, title: "Critical" },
  { id: 2, icon: HighIcon, title: "High" },
  { id: 3, icon: MediumIcon, title: "Medium" },
  { id: 4, icon: LowIcon, title: "Low" },
]
// const Priority = [
//   {id: 1, icon: CriticalIcon, title: "Critical"},
//   {id: 2, icon: HighIcon, title: "High"},
//   {id: 3, icon: MediumIcon, title: "Medium"},
//   {id: 4, icon: LowIcon, title: "Low"},
// ]

const TicketType = [
  { id: 1, icon: LowIcon, title: "Service request" },
  { id: 2, icon: HighIcon, title: "Incident" },
  { id: 3, icon: CriticalIcon, title: "Problem" },

]

const color = ['#feb1b2', '#7ac9ff', '#ffc089', '#41e590', '#ea5455', '#c8c8c8', '#656565', '#ecf1f9', '#7AC9FF']

const colors = ['#1abc9c', '#17a085', '#2ecc71', '#27ae60', '#3498db', '#2980b9', '#9b59b6', '#8e44ad', '#34495e', '#2c3e50', '#f1c40e', '#f39c12', '#d35400', '#e74c3c', '#c0392b', '#9b0000', '#f28a8a', '#00edff', '#1aa0bc', '#1cd8ff', '#ff92f4', '#d500a3', '#ffb300', '#d0cfec', '#ecf1f9', '#c8c8c8', '#656565', '#464646']


const names = [
  'Oliver Hansen',
  'Van Henry',
  'April Tucker',
  'Ralph Hubbard',
  'Omar Alexander',
  'Carlos Abbott',
  'Miriam Wagner',
  'Bradley Wilkerson',
  'Virginia Andrews',
  'Kelly Snyder',
];

const StyledMenu = withStyles({
  paper: {
    border: '1px solid #d3d4d5',
  },
})(props => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'center',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'center',
    }}
    {...props}
  />
));

const StyledMenuItem = withStyles(theme => ({
  root: {
    // '&:focus': {
    //   backgroundColor: theme.palette.primary.main,
    //   '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
    //     color: theme.palette.common.white,
    //   },
    // },
  },
}))(MenuItem);


function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      <Box p={3}>{children}</Box>
    </Typography>
  );
}

let wrapperRef;
let checkedList = [];
let checkTagsDataValue = [];
class CreateTicketDrawer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: new Date(),
      dueDate: new Date(),
      fCustomer: 'customer',
      fContact: 'contact',
      fDepartment: 'department',
      fTeam: 'team',
      fManager: 'manager',
      fAssignedTo: 'assignedTo',
      fCreatedBy: 'createdBy',
      personName: [],

      age: '',
      priority: '',
      tagsName: [],
      ticketTitle: "",
      priorities: [],
      ticketDescription: "",
      tags: [],
      users: [],
      user: 0,
      email: "",
      priority123: "",
      anchorEl: null,
      selected: '',
      text: 'before change',
      isShowDropdown: false,
      isMultiTagDropdown: false,
      isQuickCreateDropdown: false,
      isSourceDropdown: false,
      isPriorityDropdown: false,
      selectedItems: [],
      selectedPeople: [],
      selectedTeams: [],
      valueTab: 0,
      SelectedValue: 'a',
      selecteId: '',
      selecteIdTwo: '',
      ticket_source: [],
      quickTemplate: "Quick create using template",
      assignedToPeople: null,
      assignedToTeam: null,
      isEmailDropdown: false,
      teams: [],
      isEditCompDtlsOpen: false,
      isShowColorPallet: false,
      isTicketTypeDropdown: false,
      // isEmailDropdown: false,
      // uploadFile: [],
      // image: null,
      // imagePreviewUrl: ''
      file: null,
      uploadFile: [],

      userEmailCopy: 'Email',
      checkboxCopy: false,

      priorityApiData: [],
      priorityId: '',
      ticketTypeApiData: [],
      ticketTypeId: '',
      tagsApiData: [],
      checkBoxValue: [],
      assignToPlaceHolder: true,
      checkTags: true,
      checkTagsData: [],

      company: [],
      selectedCompany: {},
      departments: [],
      selectedDepartment: {},
      teamsData: [],
      selectedTeam: {},
      logedUser: {},
      selectedProject: {},
      checkedTagsArray: [],
      tagsSelect: "",
      selectedNewTagColor:""
    }
    this.onChange = this.onChange.bind(this);
  }



  componentDidMount() {
    // console.log("this.props.thisObj==",this.props.thisObj.submitTag)
    
    this.fetchTicket_source();
    this.getTemplatesData();
    this.getUserEmailsData();

    this.getAllPriorityData();
    this.getAllTagsData();

    // document.addEventListener('mousedown', this.handleClickOutsideBody);
    let requestBody1 = {
      query: `
        query getUserById($id:Int!) 
          {
            getUserById(id:$id){
              id
              username,
              emailIs,
              firstName,
              lastName,
              tenantId,
              departmentId,
              companyId,
            }
          }
          `,
      variables: {
        id: parseInt(localStorage.getItem('id')),
      }
    };

    axios({
      method: 'post',
      url: baseUrl.server,
      data: requestBody1,
      headers: {
        'Content-type': 'application/json'
      }
    }).then(res => {
      this.setState({ logedUser: res.data.data.getUserById })
      this.fetchTeams(res.data.data.getUserById.companyId, res.data.data.getUserById.departmentId, res.data.data.getUserById.tenantId);
      this.fetchcompany(res.data.data.getUserById.companyId);
      this.fetchDepartment(res.data.data.getUserById.departmentId);

      return res;
    }).catch(err => {
      console.log("Error in user==", err)
      return err;
    });
    // let templateBody = {
    //   query: `
    //     query ticketTemplate($tenantId:Int!) 
    //       {
    //         ticketTemplate(tenantId:$tenantId){
    //           templateName,id,ticketDescription,ticketPrefix,tenantId
    //           }
    //       }
    //   `,
    //   variables: {
    //     tenantId: 1,
    //   }
    // };

    // axios({
    //   method: 'post',
    //   url: baseUrl.server,
    //   data: templateBody,
    //   headers: {
    //     'Content-type': 'application/json'
    //   }
    // }).then(res => {
    //   this.setState({ templates: res.data.data.ticketTemplate })
    //   res.data.data.ticketTemplate.map(template => {
    //     QuickCreateIcons.push({ id: template.id, icon: QckCreateChck, title: template.templateName, ticketDescription: template.ticketDescription, ticketPrefix: template.ticketPrefix, tenantId: template.tenantId })
    //   })
    //   return res
    // }).catch(err => {
    //   console.log("TagsErr==", err)
    //   return err;
    // });

    let getTeamsBody = {
      query: `
        query getAllTeamsUnderTenant($tenantId:Int!) 
          {
            getAllTeamsUnderTenant(tenantId:$tenantId){
              id,
              teamName
              }
          }
      `,
      variables: {
        tenantId: 1
      }
    };

    axios({
      method: 'post',
      url: baseUrl.server,
      data: getTeamsBody,
      headers: {
        'Content-type': 'application/json'
      }
    }).then(res => {
      this.setState({ teams: res.data.data.getAllTeamsUnderTenant })
      res.data.data.getAllTeamsUnderTenant.map(team => {
        teamTabPanel.push({ id: team.id, name: team.teamName, activeIcon: profile })
      })
      return res
    }).catch(err => {
      return err;
    });


    let requestBody1Priority = {
      query: `
        query priorities 
          {
            priorities{
              id,
              priorityname
              }
          }
      `,
      // variables: {
      //   companyId: 1,
      // }
    };

    axios({
      method: 'post',
      url: baseUrl.server,
      data: requestBody1Priority,
      headers: {
        'Content-type': 'application/json'
      }
    }).then(res => {
      this.setState({ priorities: res.data.data.priorities })
      return res

    }).catch(err => {
      return err;
    });

if(this.props.propState != null){
    let requestBody2 = {
      query: `
      query allTags($companyId:Int!) 
      {
        allTags(companyId:$companyId){
          id,
           tagTitle
           color
          }
      }
  `,
      variables: {
        companyId: this.props.user.companyId,
      }
    };

    axios({
      method: 'post',
      url: baseUrl.server,
      data: requestBody2,
      headers: {
        'Content-type': 'application/json'
      }
    }).then(res => {
      this.setState({ tags: res.data.data.allTags })
      return res
    }).catch(err => {
      console.log("ErrorTags==", err)
      return err;
    });
  }else{
    this.setState({ tags: this.props.propState })
  }
    let requestBody3 = {
      query: `
    query getAllUsers 
      {
        getAllUsers{
          id,
           firstName,
           lastName,
           emailIs
          }
      }
  `,
    };

    axios({
      method: 'post',
      url: baseUrl.server,
      data: requestBody3,
      headers: {
        'Content-type': 'application/json'
      }
    }).then(res => {
      this.setState({ users: res.data.data.getAllUsers })
      res.data.data.getAllUsers.map(user => {
        // alert("user"+user.id+"ho")
        peopleTabPanel.push({ id: user.id, name: user.firstName, activeIcon: profile })

      })
      return res
    }).catch(err => {
      return err;
    });

  }
  array = [];

  checkedTags = (id) => {
    var tagString = ""
    if (this.array.includes(id)) {
      let index = this.array.indexOf(id)
      this.array.splice(index, 1);
    } else {
      this.array.push(id)
    }
    this.array.map(uniq => {
      if (this.array.length > 1) {
        tagString += " " + uniq.tagTitle
      } else {
        tagString = uniq.tagTitle
      }
    })
    this.setState({ checkedTagsArray: this.array, tagsSelect: tagString })

  }



  fetchDepartment = (deptId) => {
    const requestBody1 = {
      query: `
          query {
            getAllDepartments {
              id,
              departmentName
            }
          }
        `
    };

    fetch(baseUrl.server, {
      method: 'POST',
      body: JSON.stringify(requestBody1),
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(res => {
        if (res.status !== 200 && res.status !== 201) {
          throw new Error('Failed!');
        }
        return res.json();
      })
      .then(resData => {
        let departments = resData.data.getAllDepartments;
        if (departments != null || departments != undefined) {
          departments.map(depart => {
            if (deptId == depart.id) {
              this.setState({ selectedDepartment: depart })
            }
          })
        }
        this.setState({ departments: departments });
      })

      .catch(err => {
        console.log(err);
      });
  };

  fetchcompany = (companyId) => {
    const requestBody = {
      query: `
          query {
            getAllCompany {
              id,
              companyName,
              companyDescription
            }
          }
        `
    };

    fetch(baseUrl.server, {
      method: 'POST',
      body: JSON.stringify(requestBody),
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(res => {
        if (res.status !== 200 && res.status !== 201) {
          throw new Error('Failed!');
        }
        return res.json();
      })
      .then(resData => {
        let company = resData.data.getAllCompany;
        if (company != null || company != undefined) {
          company.map(cmp => {
            if (companyId == cmp.id) {
              this.setState({ selectedCompany: cmp })
            }
          })
        }
        this.setState({ company: company });
      })

      .catch(err => {
        console.log(err);
      });
  };

  fetchTeams = (a, b, c) => {
    const requestBody = {
      query: `
          query getTeamByCompanyIdAndDepartmentId($companyId:Int,$departmentId:Int,$tenantId:Int)
             {
              getTeamByCompanyIdAndDepartmentId(companyId:$companyId,departmentId:$departmentId,tenantId:$tenantId){
              id,
              teamName
              }
            }
          `, variables: {
        companyId: a,
        departmentId: b,
        tenantId: c
      }
    };

    axios({
      method: 'post',
      url: baseUrl.server,
      data: requestBody,
      headers: {
        'Content-type': 'application/json'
      }
    }).then(resData => {
      let teamsData = resData.data.data.getTeamByCompanyIdAndDepartmentId;
      this.setState({ teamsData: teamsData, selectedTeam: teamsData[0] });
    })
      .catch(err => {
        console.log(err);
      });
  };
  fetchTicket_source = () => {
    //this.setState({ isLoading: true });
    const requestBody = {
      query: `
            query {
              getAllticket_source{
                  id,
                  name
              }
            }
          `
    };
    fetch(baseUrl.server, {
      method: 'POST',
      body: JSON.stringify(requestBody),
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(res => {
        if (res.status !== 200 && res.status !== 201) {
          throw new Error('Failed!');
        }
        return res.json();
      })
      .then(resData => {
        // alert(JSON.stringify(ticket_source));

        const ticket_source = resData.data.getAllticket_source;
        this.setState({ ticket_source: ticket_source });
      })

      .catch(err => {
        console.log(err);
        //this.setState({ isLoading: false });
      });
  };

  getTemplatesData = async () => {
    const data = {
      tenantId: 1
    }
    const result = await getTicketTemplatesData(data);
    if (result) {
      this.setState({ templates: result })
      result.map(template => {
        QuickCreateIcons.push({ id: template.id, icon: QckCreateChck, title: template.templateName, ticketDescription: template.ticketDescription, ticketPrefix: template.ticketPrefix, tenantId: template.tenantId })
      })
    }
  }
  getUserEmailsData = async () => {
    const data = {
      companyId: 1
    }
    const result = await getUserEmails();
    if (result) {
      result.map(email => {
        Email.push({ id: email.id, icon: profile, title: email.email })
      })
    }
  }

  getAllPriorityData = async () => {
    const result = await getAllPriority(client);
    if (result) {
      if (this.state.priorityApiData == '' || this.state.priorityApiData == undefined
        || this.state.priorityApiData == null) {
        this.setState({ priorityApiData: result });
      }
    }
  }

  getAllTagsData = async () => {
    const result = await getAllTags();
    if (result) {
      if (this.state.tagsApiData == '' || this.state.tagsApiData == undefined
        || this.state.tagsApiData == null) {
        this.setState({ tagsApiData: result });
      }
    }
  }

  getAllTicketTypeData = async () => {
    const result = await getAllTicketTypes(client);
    if (result) {
      if (this.state.ticketTypeApiData == '' || this.state.ticketTypeApiData == undefined
        || this.state.ticketTypeApiData == null) {
        this.setState({ ticketTypeApiData: result });
      }
    }
  }

  handleCheckBoxSendCopy = () => {
    this.setState({ checkboxCopy: !this.state.checkboxCopy })
  }
  submitTicket = async (e) => {
    e.preventDefault();

    let requestBody = {
      query: `
        mutation addTicket(
            $name: String!, $ticketDescription: String,$ticketType:String,$priorityId:Int,
            $tenantId:Int,$userId:Int,$managerId:Int,$assignedToAgentId:Int,
            $ticketSource:String,
            $tags1:[String],
          ) 
          {
            addTicket(
              name:$name, ticketDescription:$ticketDescription,ticketType:$ticketType,priorityId:$priorityId,
             tenantId:$tenantId,userId:$userId,managerId:$managerId,assignedToAgentId:$assignedToAgentId,
              tags1:$tags1,ticketSource:$ticketSource
              )
              {
                id,
                name
              }
          }
      `,
      variables: {
        name: this.state.ticketTitle,
        ticketDescription: this.state.ticketDescription,
        ticketType: this.state.ticketType,
        priorityId: this.state.priorities[0].id,
        tenantId: 6,
        userId: parseInt(localStorage.getItem('id')),
        managerId: 4,
        assignedToAgentId: this.state.assignedToAgentId,
        tags1: this.state.tags1,
        ticketSource: this.state.ticketSource
        // user_id: this.state.user_id,
        // team_id: Number(this.state.team_id),
        // tags: this.state.tags,
        // ticket_supportive: this.state.attachment,
        // notify_others: notify_others
      }
    };

    let resData = await axios({
      method: 'post',
      url: baseUrl.server,
      data: requestBody,
      headers: {
        'Content-type': 'application/json'
      }
    }).then(res => {
      return res
    }).catch(err => {
      return err;
    });
    this.closeFDrawerHandler();
  }
  openEditCompDtlsOpen = (e) => {
    e.stopPropagation()
    this.setState({ isEditCompDtlsOpen: true })
  }
  closeCompanyDrawer = () => {
    this.setState({ isEditCompDtlsOpen: false })
  }
  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  closeFDrawerHandler = () => {
    this.props.thisObj.setState({ isCTDrawerOpen: false })
  }
  chooseStartHandler = date => {
    this.setState({ startDate: date })
  }

  chooseDueHandler = date => {
    this.setState({ dueDate: date })
  }
  toggleDrawer = (side, open) => event => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }

    this.setState({ ...this.state, [side]: open });
  };

  handleChange = event => {
    this.setState({ age: event.target.value })
  };

  priorityHandleChange = event => {
    this.setState({ priority: event.target.value })
  };
  changeHandler = (e) => {
    // alert(e.target.value)
    this.setState({ [e.target.name]: e.target.value })
  }

  multipleHandleChange = event => {
    this.setState({ tagsName: event.target.value });
  };

  quickHandleClick = (selectedObject) => {
    //set selection to the value selected
    this.setState({
      selected: selectedObject,
      ticketTitle: `${selectedObject.ticketPrefix}-${selectedObject.title}`,
      ticketDescription: selectedObject.ticketDescription,
      quickTemplate: selectedObject.title,
      isQuickCreateDropdown: false
    })
    // this.setState({ });
  }
  quickHandleClick2 = (selectedObject) => {

  }
  priorityHandleClick = (selectedObject) => {
    this.setState({ selected: selectedObject });
  }

  teamHandleClickfunction = (e, index, name) => {
    this.setState({ assignedToTeam: index, assignToPlaceHolder: false });
    if (this.state.selectedTeams.includes(name)) { } else {
      this.state.selectedTeams.push(index)
      this.state.selectedItems.push(name)
      this.setState({ assignToPlaceHolder: false })
    }
  }

  peopleHandleClickfunction1 = (e, index, name) => {
    this.setState({ [e.target.name]: index, assignToPlaceHolder: false });
    this.setState({ assignToPlaceHolder: false })
    if (this.state.selectedPeople.includes(name)) { } else {
      this.state.selectedPeople.push(name)
      this.state.selectedItems.push(name)
      this.setState({ assignToPlaceHolder: false })
    }
  }

  selectEmails = (e, index, data) => {
    this.setState({
      userEmailCopy: data.title,
      userEmailCopyId: data.id,
      isEmailDropdown: false,
    })
  }

  radioHandleChange = event => {
    this.setState(event.target.value);
  };

  tabHandleChange = (event, newValue) => {
    this.setState({ valueTab: newValue });
  };

  doChange = () => {
    this.setState({
      text: 'After change',

    })
  }

  showQuickCreateDropdown = () => {
    this.setState({ isQuickCreateDropdown: !this.state.isQuickCreateDropdown })
  }

  showDropdown = () => {
    this.setState({ isShowDropdown: !this.state.isShowDropdown })
  }

  showMultiTagDropdown = () => {
    this.setState({ isMultiTagDropdown: !this.state.isMultiTagDropdown })
  }

  showSourceDropdown = () => {
    this.setState({ isSourceDropdown: !this.state.isSourceDropdown })
  }

  showPriorityDropdown = () => {
    this.setState({ isPriorityDropdown: !this.state.isPriorityDropdown })
  }

  showEmailDropdown = () => {
    this.setState({ isEmailDropdown: !this.state.isEmailDropdown })
  }

  showTicketTypeDropdown = () => {
    this.setState({ isTicketTypeDropdown: !this.state.isTicketTypeDropdown })
  }

  showColorPalletPopup = () => this.setState({ isShowColorPallet: !this.state.isShowColorPallet });

  handleClickOutside = (event) => {
    if (wrapperRef && !wrapperRef.contains(event.target)) {
      this.setState({ isShowColorPallet: false })
    }
  }

  handleOutsideClickClosedrp=()=>{
    if(!this.state.isQuickCreateDropdown){
      document.addEventListener('click', this.handleOutsideClickClosedrp, false);
    }else{
      document.removeEventListener('click', this.handleOutsideClickClosedrp, false);
    }
    this.setState(prevState => ({
      isQuickCreateDropdown: !prevState.isQuickCreateDropdown,
   }));
  }

  handleOutsideClickClosedPriority=()=>{
    if(!this.state.isPriorityDropdown){
      document.addEventListener('click', this.handleOutsideClickClosedPriority, false);
    }else{
      document.removeEventListener('click', this.handleOutsideClickClosedPriority, false);
    }
    this.setState(prevState => ({
      isPriorityDropdown: !prevState.isPriorityDropdown,
   }));
  }

  handleOutsideClickClosedTcktTyp=()=>{
    if(!this.state.isTicketTypeDropdown){
      document.addEventListener('click', this.handleOutsideClickClosedTcktTyp, false);
    }else{
      document.removeEventListener('click', this.handleOutsideClickClosedTcktTyp, false);
    }
    this.setState(prevState => ({
      isTicketTypeDropdown: !prevState.isTicketTypeDropdown,
   }));
  }

  handleOutsideClickClosedTags=()=>{
    if(!this.state.isMultiTagDropdown){
      document.addEventListener('click', this.handleOutsideClickClosedTags, false);
    }else{
      document.removeEventListener('click', this.handleOutsideClickClosedTags, false);
    }
    this.setState(prevState => ({
      isMultiTagDropdown: !prevState.isMultiTagDropdown,
   }));
  }

  handleOutsideClickClosedSource=()=>{
    if(!this.state.isSourceDropdown){
      document.addEventListener('click', this.handleOutsideClickClosedSource, false);
    }else{
      document.removeEventListener('click', this.handleOutsideClickClosedSource, false);
    }
    this.setState(prevState => ({
      isSourceDropdown: !prevState.isSourceDropdown,
   }));
  }

  handleOutsideClickClosedEmail=()=>{
    if(!this.state.isEmailDropdown){
      document.addEventListener('click', this.handleOutsideClickClosedEmail, false);
    }else{
      document.removeEventListener('click', this.handleOutsideClickClosedEmail, false);
    }
    this.setState(prevState => ({
      isEmailDropdown: !prevState.isEmailDropdown,
   }));
  }

  handleOutsideClickClosedAssign=()=>{
    if(!this.state.isShowDropdown){
      document.addEventListener('click', this.handleOutsideClickClosedAssign, false);
    }else{
      document.removeEventListener('click', this.handleOutsideClickClosedAssign, false);
    }
    this.setState(prevState => ({
      isShowDropdown: !prevState.isShowDropdown,
   }));
  }

  handleOutsideBdy(e){
    // ignore clicks on the component itself
    if (this.node.contains(e.target)) {
      return;
    }
    
    this.handleOutsideClickClosedrp();
    this.handleOutsideClickClosedPriority();
    this.handleOutsideClickClosedTcktTyp();
    this.handleOutsideClickClosedTags();
    this.handleOutsideClickClosedSource();
    this.handleOutsideClickClosedEmail();
    this.handleOutsideClickClosedAssign();
  }

  handleClickOutsideBody = async (event) => {
    if (wrapperRef && !wrapperRef.contains(event.target)) {
      await this.setState({
        // isTicketTypeDropdown: false,
        // isQuickCreateDropdown: false,
        // isMultiTagDropdown: false,
        // isSourceDropdown: false,
        // isPriorityDropdown: false,
        // isEmailDropdown: false,
        // isShowDropdown: false,
      })
      // this.refreshCells()
    }
  }

  setWrapperRef = (node) => wrapperRef = node;
  handleChangeComplete = (e)=>{
    this.setState({selectedNewTagColor:e.hex,isShowColorPallet:false})

  }

  colorPalletPopup = () => (
    <div className='color-pallet-popup-for-tags' ref={this.setWrapperRef}>
      <span>Color palette</span>
      <TwitterPicker colors={colors} onChangeComplete={this.handleChangeComplete} />
    </div>
  )


  // onChangeAttachHandler=event=>{
  //   console.log('onChangeAttachHandler', event.target.files)
  //   let file = [event.target.files[0]];
  //   this.setState({
  //     uploadFile: file
  //   })
  //   console.log("images",  event.target.files)
  // }

  onChange = (event) => {
    let imageList = [];
    let extension = null;
    for (let i = 0; i < event.target.files.length; i++) {
      extension = event.target.files[i].name.split('.')[1];
      if (extension === 'jpg' || extension === 'jpeg' || extension === 'jpg' || extension === 'bmp' || extension === 'png' || extension === 'svg') {
        this.imageToBase64Converter(event.target.files[i], result => {

          imageList = [...imageList, result]
          this.setState({
            uploadFile: [...this.state.uploadFile, result]
          })
        })
      }
    }
  }


  imageToBase64Converter = (image, callback) => {
    let reader = new FileReader();
    reader.readAsDataURL(image);
    reader.onload = () => {
      callback(reader.result)
    }
  }

 submitTag=(event)=>{
    if(this.state.selectedNewTagColor){
    this.props.thisObj.submitTag1(event.target.value,this.state.selectedNewTagColor,result=>{
      let array =[]
      array = this.state.tags
      array.push(result.data.addTag);
      this.setState({tags:array})
    })
    this.setState({enterTag:"",selectedNewTagColor:""})
  }else{
      alert("select color for new tag")
    }
  
}

submitTag1=()=>{

  if(this.state.selectedNewTagColor){
    this.props.thisObj.submitTag1(this.state.enterTag,this.state.selectedNewTagColor,result=>{
      let array =[]
      array = this.state.tags
      array.push(result.data.addTag);
      this.setState({tags:array})
    })
    this.setState({enterTag:"",selectedNewTagColor:""})
  }else{
      alert("select color for new tag")
    }
}

  // Added on Nov 18th

  handleChangePriority = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
      isPriorityDropdown: false,
      isTicketTypeDropdown: false
    });
  }

  handleCheckBoxTags = (event, data) => {
    if (event.target.checked) {
      checkedList.push(Number(event.target.value));
      checkTagsDataValue.push(data.tagTitle);
      this.setState({ checkBoxValue: checkedList, checkTags: false, checkTagsData: checkTagsDataValue });
    } else if (!event.target.checked) {
      checkedList.pop(Number(event.target.value));
      checkTagsDataValue.pop(data.tagTitle);
      this.setState({ checkBoxValue: checkedList, checkTagsData: checkTagsDataValue });
    }
  }

  sideList = side => (
    <div className={'create-ticket-drawer-bodySection'} role="presentation"  ref={node => { this.node = node; }}>
      {this.state.isEditCompDtlsOpen ? <EditCompanyDetails onClick={(e) => e.stopPropagation()} isEditCompDtlsOpen={this.state.isEditCompDtlsOpen} allState={this.state} thisObj={this} /> : null}
      <div className="create-ticket-left-section">
        <img src={CreateTicketDrawerClose} alt='' onClick={this.closeFDrawerHandler} />
      </div>
      <div className="crete-ticket-body">
        <div className="create-ticket-body-component">
          <div className='create-ticket-drawer-middle-section'>
            <div className="create-ticket-header-section d-flex justify-space-between">
              <div className="create-ticket-header-text">
                <p className="create-ticket-logo-text">CREATE TICKET</p>
                <p className="create-ticket-logo-sub-text">Lorem ipsum is simply dummy text of the printing and <br></br>typesetting industry</p>
              </div>
              <div className="create-ticket-header-drop-down d-flex justify-flex-end">
                {/* <StyledMenu
                  id="customized-menu"
                  anchorEl={this.state.anchorEl}
                  keepMounted
                  open={Boolean(this.state.anchorEl)}
                  onClose={this.handleClose}
                  className="customized-ticket"

                >
                  <div className="quick-create-search-bar d-flex align-items-center">
                    <InputBase
                      placeholder="Search of template"
                      className="search-input"
                      inputProps={{ 'aria-label': 'search' }}
                    />
                    <SearchIcon />
                  </div>
                  <Scrollbars className="custom-scroll" style={{ height: 130 }}>
                    {QuickCreateIcons.map(icon =>
                      <StyledMenuItem className="customized-ticket-list"
                        onClick={() => this.quickHandleClick(icon)} onClose={this.handleClose}
                      >
                        <div className="quick-create-ticket-one d-flex">
                          <img src={icon.icon}></img>
                          <p>{icon.title}</p>
                        </div>
                      </StyledMenuItem>
                    )}
                  </Scrollbars>
                </StyledMenu> */}
                {/* Comented ends */}

                <div className='menu-container'>
                  <div className='quick-create-template'>
                    <div className='selected-item-container'>
                      <p className="quck-templt-text">{this.state.quickTemplate}</p>
                    </div>
                    <div className="quick-drp-dwn-img">
                      <img src={DropDwnIcon} onClick={this.handleOutsideClickClosedrp} />
                    </div>
                  </div>
                  <div className={`dropdown-menu-container ${this.state.isQuickCreateDropdown ? 'open-dropdown'
                    : 'close-dropdown'}`} >
                  
                    <div className="quick-create-search-bar d-flex align-items-center">
                      <InputBase
                        placeholder="Search of template"
                        className="search-input"
                        inputProps={{ 'aria-label': 'search' }}
                      />
                      <SearchIcon />
                    </div>
                    <Scrollbars className="custom-scroll" style={{ height: 160 }}>
                      {QuickCreateIcons ? QuickCreateIcons.map(icon =>
                        <StyledMenuItem className="customized-ticket-list" onClick={() => this.quickHandleClick(icon)} onClose={this.handleClose}>
                          <div className="quick-create-ticket-one d-flex">
                            <img src={icon.icon}></img>
                            <p>{icon.title}</p>
                          </div>

                        </StyledMenuItem>
                      ) : ''}
                    </Scrollbars>
                  </div>
                </div>
              </div>
            </div>
            <div className="create-ticket-drawer-title">
              <form noValidate autoComplete="off" className="ticket-title-form">
                <TextField
                  required
                  id="outlined-error"
                  label="Ticket Title"
                  name="ticketTitle"
                  className=""
                  value={this.state.ticketTitle}
                  margin="normal"
                  onChange={this.changeHandler}
                />
              </form>
            </div>
            <div className="create-ticket-drawer-ticket-type-priority d-flex justify-space-between">
              <div className="create-ticket-drawer-ticket-type">
                <div className='menu-container'>
                  <div className='ticket-drawer-container' onClick={this.showTicketTypeDropdown}>
                    <div className='selected-item-container'>
                      <p>
                        {
                          this.state.ticketTypeId == 1 ? "Service request" :
                            this.state.ticketTypeId == 2 ? "Incident" :
                              this.state.ticketTypeId == 3 ? "Problem" : "Ticket Type"
                        }
                      </p>
                    </div>
                    <div className="ticket-drp-dwn-img">
                      <img src={DrpDwnIcn} onClick={this.handleOutsideClickClosedTcktTyp} />
                    </div>

                  </div>

                  <div className={`ticket-type-dropdown-menu-container 
                    ${this.state.isTicketTypeDropdown ? 'ticket-type-open-dropdown' : 'close-dropdown'}`}
                    ref={this.setWrapperRef}
                  >
                    {TicketType.map((icon, index) =>
                      <StyledMenuItem className="customized-ticket-source"
                        onClick={() => this.priorityHandleClick(icon)} onClose={this.handleClose}
                      >
                        <div className="create-ticket-one-tckt-typ d-flex">
                          <div className="create-ticket-tckt-img-text d-flex">
                            <img src={icon.icon}></img>
                            <p>{icon.title}</p>
                          </div>

                          <div className="create-ticket-src-radio">
                            <Radio
                              checked={icon.id == this.state.ticketTypeId}
                              // onChange={(e) => this.peopleHandleClickfunction(e, index)}
                              value={icon.id}
                              name="ticketTypeId"
                              color="primary"
                              inputProps={{ 'aria-label': '' }}
                              onChange={this.handleChangePriority}
                            />
                          </div>
                        </div>

                      </StyledMenuItem>
                    )}

                  </div>
                </div>
              </div>
              <div className="create-ticket-drawer-priority">
                <div className='menu-container'>
                  <div className='priority-container' onClick={this.showPriorityDropdown}>
                    <div className='selected-item-container'>
                      <p htmlFor="priority" name="priority">
                        {
                          this.state.priorityId == 1 ? "Low" :
                            this.state.priorityId == 2 ? "Medium" :
                              this.state.priorityId == 3 ? "High" :
                                this.state.priorityId == 4 ? "Critical" : "Priority"
                        }
                      </p>
                    </div>
                    <div className="priority-drp-dwn-img">
                      <img src={DrpDwnIcn} onClick={this.handleOutsideClickClosedPriority} />
                    </div>

                  </div>

                  <div className={`priority-dropdown-menu-container ${this.state.isPriorityDropdown ? 'priority-open-dropdown' : 'close-dropdown'}`} ref={this.setWrapperRef}>
                    {
                      this.state.priorityApiData ? this.state.priorityApiData.map((icon, index) =>
                        <StyledMenuItem className="customized-ticket-source"
                          onClick={() => this.priorityHandleClick(icon)} onClose={this.handleClose}
                        >
                          <div className="create-ticket-one-priority d-flex">
                            <div className="create-ticket-prio-img-text d-flex">
                              {/* <img src={icon.icon}></img> */}
                              <p value={icon.id}>{icon.priorityname}</p>
                            </div>

                            <div className="create-ticket-src-radio">
                              <Radio
                                checked={icon.id == this.state.priorityId}
                                onChange={this.handleChangePriority}
                                value={icon.id}
                                name="priorityId"
                                color="primary"
                                inputProps={{ 'aria-label': '' }}
                              />
                            </div>
                          </div>

                        </StyledMenuItem>
                      ) : ''
                    }

                  </div>
                </div>
              </div>
            </div>

            <div className="create-ticket-drawer-text-area">

              <TextareaAutosize
                // aria-label="minimum height"
                rows={3}
                placeholder="Ticket description"
                name="ticketDescription"
                value={this.state.ticketDescription}
                onChange={this.changeHandler}
              />
            </div>

            <div className="create-ticket-drawer-tags-source d-flex justify-space-between">
              <div className="create-ticket-drawer-tags">
                <div className='menu-container'>
                  <div className='tags-container' >
                    <div className='selected-item-container'>
                      {this.state.tagsSelect != "" ?
                        <p className="tag-text">{this.state.tagsSelect}</p>
                        : <p>Tags</p>}
                    </div>
                    <div className="tag-drp-dwn-img" >
                      <img src={AddMultipleTag} onClick={this.handleOutsideClickClosedTags} />
                    </div>

                  </div>

                  <div className={`tag-dropdown-menu-container ${this.state.isMultiTagDropdown ? 'tag-open-dropdown' : 'close-dropdown'}`}>

                    <Scrollbars className="custom-scroll">
                      {this.state.tags ? this.state.tags.map((icon, index) =>
                        <StyledMenuItem className="customized-ticket-source" onClose={this.handleClose}>
                          <div className="create-ticket-one-tag d-flex">
                            <div className="create-ticket-tag-img-text d-flex">
                              <div className="create-ticket-tag-check">
                                <Checkbox
                                  className="create-ticket-tags-chck"
                                  name="checkedTagsArray"
                                  value={this.state.checkedTagsArray}
                                  color="primary"
                                  onClick={() => this.checkedTags(icon)}
                                />
                              </div>
                              <p>{icon.tagTitle}</p>
                            </div>
                            <img 
                              src={icon.id == 1 ? MediumIcon : icon.id == 2 ?
                                CriticalCustomerIcon : icon.id == 3 ? HighIcon : icon.id == 4 ?
                                  LowIcon : icon.id == 5 ? MediumIcon : null
                              }>
                            </img>
                            {/* <img src={icon.tagTitle == "Risk" ? MediumIcon : (icon.tagTitle == "Completed") ? HighIcon : null }></img> */}
                          </div>

                        </StyledMenuItem>
                      ) : null}
                    </Scrollbars>
                    <hr></hr>
                    <div className="create-ticket-search-tag d-flex justify-space-between">
                      <InputBase
                        value={this.state.enterTag}
                        placeholder="Enter tag name"
                        inputProps={{'aria-label': 'search' , style:{color:`${this.state.selectedNewTagColor}`}}}
                        className="search-input"
                        onChange={(e)=>this.setState({enterTag:e.target.value})}
                        onKeyPress={(event) =>{ if(event.key=="Enter"){this.submitTag(event)
                        }
                      }}
                      />
                      <img src={SearchTagAdd} alt="" onClick={this.submitTag1}></img>
                    </div>
                    <div className="create-tckt-drawer-color-picker d-flex">
                      <img src={ClrPckrTray} alt="" onClick={this.showColorPalletPopup}></img>
                      {this.state.isShowColorPallet ? this.colorPalletPopup() : null}
                      <div className="ticket-circle-color-picker">
                        {/* <CirclePicker/> */}
                        <CirclePicker colors={color} circleSize={18} width="260px" circleSpacing={10} onChangeComplete={this.handleChangeComplete} />
                      </div>
                    </div>
                  </div>
                </div>
                {/* <FormControl className="">
                        <InputLabel htmlFor="select-multiple">Tags</InputLabel>
                        <div className="create-ticket-drawer-menu-item">
                            <Select
                                multiple
                                value={this.state.tagsName}
                                name="tagsName"
                                onChange={this.multipleHandleChange}
                                input={<Input id="select-multiple" />}
                            >
                              {
                                this.state.tags.map(tag=>{
                                  return <MenuItem value={tag.id}>{tag.tagTitle}</MenuItem>
                                })
                              }
                            </Select>
                        </div>
            </FormControl> */}
                {/* <div className='menu-container'>
                <div className='custome-multiple-tag'>
                  <div className='selected-item-container'>
                    <p>Tags</p>
                  </div><img src={AddMultipleTag} onClick={this.showMultiTagDropdown}/>
                </div>

                <div className={`dropdown-menu-container ${this.state.isMultiTagDropdown ? 'open-dropdown' : 'close-dropdown'}`}>
                    {
                      Tags.map((icon, index)=> 
                          <StyledMenuItem className="customized-ticket-source" onClick={()=> this.quickHandleClick(icon)} onClose={this.handleClose}>
                          <div className=".create-ticket-one-tags d-flex">
                            <div className="create-ticket-tags-img-text d-flex">
                            <Checkbox
                                className="create-ticket-tags-chck"
                                value="checkedB"
                                color="primary"
                            />
                              <p>{icon.title}</p>
                            </div>
                              
                            
                          </div>
                          
                        </StyledMenuItem>
                      )
                    }
                </div>
            </div> */}
              </div>
              <div className="create-ticket-drawer-source">
                <div className='menu-container'>
                  <div className='Source-container'>
                    <div className='selected-item-container'>
                      <p>Source</p>
                    </div>
                    <div className="src-drp-dwn-img">
                      <img src={DrpDwnIcn} onClick={this.handleOutsideClickClosedSource} />
                    </div>

                  </div>

                  <div className={`dropdown-menu-container ${this.state.isSourceDropdown ? 'open-dropdown' : 'close-dropdown'}`} ref={this.setWrapperRef}>

                    {this.state.ticket_source != null || this.state.ticket_source != undefined ?
                      this.state.ticket_source.map((icon, index) =>
                        <StyledMenuItem className="customized-ticket-source" onClick={() => this.quickHandleClick2(icon)} onClose={this.handleClose}>
                          <div className="create-ticket-one-source d-flex">
                            <div className="create-ticket-src-img-text d-flex">
                              <img src={icon.icon}></img>
                              <p>{icon.name}</p>
                            </div>

                            <div className="create-ticket-src-radio">
                              <Radio
                                checked={this.state.selecteIdTwo === index}
                                value=""
                                name="radio-button-demo"
                                color="primary"
                                inputProps={{ 'aria-label': '' }}
                              // onChange={(e) => this.peopleHandleClickfunction(e, index)}
                              />
                            </div>
                          </div>

                        </StyledMenuItem>

                      ) : null}

                  </div>
                </div>
              </div>

            </div>

            {/* <div className="create-ticket-drawer-attachment-container">
                        <p>ATTACHMENTS</p>
                        <div className="create-ticket-drawer-custom-file-upload">
                            <Button>
                                <input
                                    type="file"
                                    className="custom-file-input"
                                    id="inputGroupFile01"
                                />
                            </Button>
                            <div className="custom-file-label-upload-image-icon">
                                <img src={AttachmentAddIcon}></img>
                                <p className="custom-file-label-upload-text-one">Drag attach file.</p>
                                <p className="custom-file-label-upload-text-two">or <span>browse</span></p>
                            </div>

                            <label className="custom-file-label-upload" htmlFor="inputGroupFile01">

                            </label>
                        </div>
                    </div> */}

            {/* <div className='create-ticket-attachment'>
                      <span>Attachments</span>
                      
                      <div className='create-tckt-file-attach-section'>
                        
                      {this.state.uploadFile.map((file) =>{
                         
                         return <img src={file}></img>
                       })}
                       <div className={`create-ticket-file-upload-field ${this.state.uploadFile.length === 0 ? 'file-attach-full-width' : 'file-upload-after-width'} `}>
                          <Button className='create-ticket-file-attach-button'>
                              <input
                                  type={'file'}
                                  className="custom-attach-file-input "
                                  id="inputGroupFile01"
                                  onChange={this.onChangeAttachHandler}
                                  multiple={true}
                              />
                          </Button>
                       </div>
                      
                          <p>Drag attach file, <br/>or <span>browse</span></p>
                      </div>
                    </div> */}
            
              {/* <input type="file" onChange={this.onChange} />
  
          <img style={{ width: "50%" }} src={this.state.file} /> */}
              <div className='create-ticket-attachment'>
                <p className="create-ticket-attachment-header-text">Attachments</p>

                <div className="uploaded-image-and-attach">
                  {this.state.uploadFile ? this.state.uploadFile.map((file) => {
                    return <img src={file}></img>
                  }) : ''}
                  <div className='create-tckt-file-attach-section'>

                    <div className={`create-ticket-file-upload-field ${this.state.uploadFile.length === 0 ? 'file-attach-full-width' : 'file-upload-after-width'} `}>
                      <Button className='create-ticket-file-attach-button'>
                        <input
                          type='file'
                          className="custom-attach-file-input "
                          id="inputGroupFile01"
                          onChange={this.onChange}
                          multiple={true}
                        />
                        <p className="drag-attach-text">Drag attach file, <br />or <span>browse</span></p>

                      </Button>
                    </div>

                  </div>
                  <div className="attach-drg-text-container">
                  </div>
                </div>

              </div>
            
          </div>

          <div className='create-ticket-drawer-right-section'>
            <div className="create-ticket-drawer-body-right">
              <div className="crt-ticket-drawer-company-details">
                <div className="crt-ticket-company-description d-flex justify-space-between">
                  <div className="create-tckt-cmpny-img d-flex">
                    <img src={CompanyLogo}></img>
                    <div className="create-ticket-drawer-company-text">
                      <p className="create-ticket-drawer-company-text-one">{this.state.selectedCompany.companyName}</p>
                      <p className="create-ticket-drawer-company-text-two">{this.state.selectedCompany.companyDescription}</p>
                      
                    </div>
                  </div>
                  <div className="crte-tckt-drawer-company-details-edit">
                    <img src={CompanyEditIcon} onClick={(e) => this.openEditCompDtlsOpen(e)}></img>
                  </div>
                </div>
                <div className="dprtmt-and-team-name">
                  {this.state.selectedDepartment ? <p className="deprt-name">{this.state.selectedDepartment.departmentName}</p> : <p>Department Name</p>}
                  {
                    this.state.selectedTeam.teamName ? <p className="tm-name">{this.state.selectedTeam.teamName}</p> : <p className="tm-name">Team Name</p>
                  }
                </div>
                {/* <p className="project-name">Project name</p> */}


              </div>

              <div className="create-ticket-drawer-assign-to">
                {/* <FormControl required className="">
                              <InputLabel htmlFor="user">Assign To</InputLabel>
                              <Select
                              value={this.state.user}
                              onChange={this.changeHandler}
                              name="user"
                              inputProps={{
                                  id: 'age-required',
                              }}
                              className=""
                              >
                              {this.state.users.map(user=>{
                              return <MenuItem value={user.id}>{user.firstName} {user.lastName}</MenuItem>
                              })
                              }
                              </Select>
                              
                      </FormControl> */}
                <div className='menu-container'>
                  <div className={`dropdown-menu-container ${this.state.isShowDropdown ? 'open-dropdown' : 'close-dropdown'}`} ref={this.setWrapperRef}>
                    <Tabs value={this.state.valueTab} onChange={this.tabHandleChange} aria-label="simple tabs example">
                      <Tab label="Teams" />
                      <Tab label="People" />
                    </Tabs>
                    <TabPanel value={this.state.valueTab} index={0}>
                      <div className="tab-panel-container-search-bar d-flex">
                        <InputBase
                          placeholder="Search teams"
                          className="search-input"
                          inputProps={{ 'aria-label': 'search' }}
                        />
                        <SearchIcon />
                      </div>

                      <div className="tab-panel-container">
                        <Scrollbars className="custom-scroll" style={{ height: 130 }}>
                          {this.state.teams != null || this.state.teams != undefined ?
                            this.state.teams.map((team, index) => (

                              <div className="tab-panel-one d-flex justify-space-between">
                                <div className="tab-panel-one-img d-flex justify-content-center align-items-center">
                                  <img src={profile}></img>
                                  <p className="assign-to-profile-name">{team.teamName}</p>
                                </div>
                                <Radio
                                  checked={this.state.assignedToTeam === team.id}
                                  onChange={(e) => this.teamHandleClickfunction(e, team.id, team.teamName)}
                                  value=""
                                  name="radio-button-demo"
                                  color="primary"
                                  inputProps={{ 'aria-label': '' }}
                                />
                              </div>
                            )) : null
                          }
                        </Scrollbars>
                      </div>
                    </TabPanel>


                    <TabPanel value={this.state.valueTab} index={1}>
                      <div className="tab-panel-container-search-bar d-flex">
                        <InputBase
                          placeholder="Search people"
                          className="search-input"
                          inputProps={{ 'aria-label': 'search' }}
                        />
                        <SearchIcon />
                      </div>
                      <div className="tab-panel-container">
                        <Scrollbars className="custom-scroll" style={{ height: 130 }}>
                          {this.state.users != null || this.state.users != undefined ?
                            this.state.users.map((user, index) => (

                              <div className="tab-panel-one d-flex justify-space-between">
                                <div className="tab-panel-one-img d-flex justify-content-center align-items-center">
                                  <img src={profile}></img>
                                  <p className="assign-to-profile-name">{user.firstName}</p>
                                </div>
                                <Radio
                                  checked={this.state.assignedToPeople === user.id}
                                  onChange={(e) => this.peopleHandleClickfunction1(e, user.id, user.firstName)}
                                  value=""
                                  name="assignedToPeople"
                                  color="primary"
                                  inputProps={{ 'aria-label': '' }}
                                />
                              </div>
                            )) : null
                          }
                        </Scrollbars>
                      </div>
                    </TabPanel>
                  </div>
                  <div className='custome-non-library-multi'>
                    <div className='selected-item-container'>
                      {
                        this.state.assignToPlaceHolder ?
                          <p className="create-drop-assign-to"><img src={profile} /> Assign To</p> : ''
                      }
                      {
                        this.state.selectedItems ? this.state.selectedItems.map(item =>
                          <div className='selected-item-template d-flex'>
                            <img src={profile} />
                            <div>
                              <p className="create-drop-profile-name">{item}</p>
                            </div>
                          </div>
                        ) : ''}
                    </div>
                    <img src={DropDwnIcon} onClick={this.handleOutsideClickClosedAssign} />
                  </div>

                </div>
              </div>
              <div className="snd-cpy-check-box">
                <Checkbox
                  value="checkedB"
                  color="primary"
                  onChange={this.handleCheckBoxSendCopy}
                />
                <span className="snd-cpy">Send a copy</span>
              </div>

              <div className="create-ticket-drawer-email">
                <div className='menu-container'>

                  <div className='create-ticket-email'>
                    <div className={`${this.state.checkboxCopy == false ? 'disabled' : ''} selected-item-container`}>
                      <img src={CreateEmailIcon} alt=""></img>
                      <p className="eml-text">{this.state.checkboxCopy ? this.state.userEmailCopy : 'Email'}</p>
                    </div>
                    <div className="email-drp-dwn-img">
                      <img src={DrpDwnIcn} onClick={this.handleOutsideClickClosedEmail} />
                    </div>

                  </div>
                  <div className={`dropdown-menu-container ${this.state.isEmailDropdown ? 'open-dropdown' : 'close-dropdown'}`}>
                    <Scrollbars className="custom-scroll" style={{ height: 155 }}>
                      {Email ? Email.map((icon, index) =>
                        <StyledMenuItem className="customized-ticket-source"
                          // onClick={() => this.quickHandleClick(icon)} 
                          onClick={(e) => this.selectEmails(e, index, icon)}
                          onClose={this.handleClose}
                        >
                          <div className="create-ticket-one-email d-flex">
                            <div className="create-ticket-email-img-text d-flex">
                              <img src={icon.icon}></img>
                              <p>{icon.title}</p>
                            </div>

                            <div className="create-ticket-email-radio">
                              <Radio
                                checked={this.state.userEmailCopyId === icon.id}
                                onChange={(e) => this.selectEmails(e, index, icon)}
                                value=""
                                name="radio-button-demo"
                                color="primary"
                                inputProps={{ 'aria-label': '' }}
                                className="email-radio-button"
                              />
                            </div>
                          </div>
                        </StyledMenuItem>
                      ) : null}
                    </Scrollbars>
                  </div>

                </div>
                {/* <FormControl required className="">
                              <InputLabel htmlFor="email">Email</InputLabel>
                        
                              <Select
                              value={this.state.email}
                              onChange={this.changeHandler}
                              name="email"
                              inputProps={{
                                  id: 'age-required',
                              }}
                              className=""
                              >
                                {
                                  this.state.users.map(user=>{
                                  return <MenuItem value={user.emailIs}>{user.emailIs}</MenuItem>
                                  })
                                }
                              </Select>
                              
                      </FormControl> */}
              </div>


              <div className="create-ticket-drawer-knowledge-container">
                <div className="create-ticket-drawer-knowledge-container-two">
                  <div className="knowledge-base d-flex justify-space-between">
                    <div className="knowledge-base-header d-flex">
                      <img src={KnowledgeIcon} alt="knowledge"></img>
                      <p className="knowledge-header-text">KNOWLEDGE BASE</p>
                    </div>

                    <div className="knowledge-search">
                      <img src={KnowledgeSearch} alt=""></img>
                    </div>
                  </div>
                  <div className="knowledge-questions">
                    <div className="knowledge-base-value"><p>01</p></div>
                    <div className="knwldg-text">
                      <p className="knwldg-text-1st">How to create a new project for </p>
                      <p className="knwldg-text-next">implementation?</p>
                    </div>
                  </div>
                  <div className="knowledge-questions">
                    <div className="knowledge-base-value"><p>02</p></div>
                    <div className="knwldg-text">
                      <p className="knwldg-text-1st">How to invite a new user for support</p>
                      <p className="knwldg-text-next">management tool?</p>
                    </div>
                  </div>
                  <div className="knowledge-questions">
                    <div className="knowledge-base-value"><p>03</p></div>
                    <div className="knwldg-text">
                      <p className="knwldg-text-1st">How to create a new project for </p>
                      <p className="knwldg-text-next">implementation?</p>
                    </div>

                  </div>
                  <div className="knowledge-questions">
                    <div className="knowledge-base-value"><p>04</p></div>
                    <div className="knwldg-text">
                      <p className="knwldg-text-1st">How to invite a new user for support </p>
                      <p className="knwldg-text-next">management tool?</p>
                    </div>

                  </div>

                  <div className="create-drawer-sell-All">
                    <p>See All</p>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
        <hr></hr>
        <div className='create-ticket-drawer-bottom-buttons d-flex justify-space-between'>
          <div className="create-ticket-drawer-save-as-template">
            <Button variant="contained" className="ch-save-button">
              Save as Template
            </Button>
          </div>

          <div className="create-ticket-drawer-submit d-flex justify-flex-end">
            <Button variant="contained" onClick={this.submitTicket} className="ch-submit-button">
              Submit
            </Button>
          </div>
        </div>
      </div>



    </div>
  );

  render() {
    return (
      <Drawer onClick={this.closeCompanyDrawer} anchor="right" open={this.props.isCTDrawerOpen} onClose={this.toggleDrawer('right', false)} className='create-ticket-drawer'>
        {this.sideList('right')}
      </Drawer>
    )
  }

}

export default CreateTicketDrawer;
