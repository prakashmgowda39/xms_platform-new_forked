import React from 'react';
import './EditCompanyDetails.scss';
  
import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
// import List from '@material-ui/core/List';
// import Divider from '@material-ui/core/Divider';
// import ListItem from '@material-ui/core/ListItem';
// import ListItemIcon from '@material-ui/core/ListItemIcon';
// import ListItemText from '@material-ui/core/ListItemText';
// import InboxIcon from '@material-ui/icons/MoveToInbox';
// import MailIcon from '@material-ui/icons/Mail';

// import DatePicker from 'react-date-picker';
// import moment from 'moment';

import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

import axios from "axios";
import { baseUrl } from "../../../../constants";

import companyIcon from '../../../../assets/editCompanyDrawer/fav-icon-white.svg'
import departmentIcon from '../../../../assets/editCompanyDrawer/Icon awesome-user-circle.svg'
import teamIcon from '../../../../assets/editCompanyDrawer/Icon material-group-add.svg'
import projectIcon from '../../../../assets/editCompanyDrawer/Icon awesome-product-hunt.svg'

const names = [
  'Oliver Hansen',
  'Van Henry',
  'April Tucker',
  'Ralph Hubbard',
  'Omar Alexander',
  'Carlos Abbott',
  'Miriam Wagner',
  'Bradley Wilkerson',
  'Virginia Andrews',
  'Kelly Snyder',
];

class FiltersDrawer extends React.Component {
  constructor(props) {
    super(props);
    this.companyNameField = React.createRef();
    this.state = {
      startDate: new Date(),
      dueDate: new Date(),
      //componyName: 'Xcelpros LLC',
      department:[],
      team: [],
      projects: [],
      taskProjectid: '',
      teamId: [],
      tasks:[],
      company:[],
      habits:[],users:[],
      selectedDept:{},
      selectedcmp:{},
      selectedTm:{},
      selectedPrj:{},
    }
  }
  componentDidMount(){
    document.addEventListener('mousedown', this.handleClickOutside);
    this.setState({department:this.props.allState.departments,
      team: this.props.allState.teamsData,
      company:this.props.allState.company,
      selectedDept:this.props.allState.selectedDepartment,
selectedcmp:this.props.allState.selectedCompany,
selectedTm:this.props.allState.selectedTeam,
    })
// this.fetchdepartment();
this.fetchproject();
// this.fetchteam();
this.fetchtasks();
this.fetchHabits();
// this.fetchcompany();
this.fetchUsers();
  }
  fetchdepartment = () => {
    const requestBody = {
        query: `
          query {
            getAllDepartments {
              departmentName,
            }
          }
        `
    };

    fetch(baseUrl.server, {
        method: 'POST',
        body: JSON.stringify(requestBody),
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(res => {
            if (res.status !== 200 && res.status !== 201) {
                throw new Error('Failed!');
            }
            return res.json();
        })
        .then(resData => {
           
            let department = resData.data.getAllDepartments;
            this.setState({ department: department });
        })

        .catch(err => {
            console.log(err);
        });
       

};

fetchcompany = () => {
  const requestBody = {
      query: `
        query {
          getAllCompany {
            id,
            companyName
          }
        }
      `
  };

  fetch(baseUrl.server, {
      method: 'POST',
      body: JSON.stringify(requestBody),
      headers: {
          'Content-Type': 'application/json'
      }
  })
      .then(res => {
          if (res.status !== 200 && res.status !== 201) {
              throw new Error('Failed!');
          }
          return res.json();
      })
      .then(resData => {
          let company = resData.data.getAllCompany;
          this.setState({ company: company });
      })
      .catch(err => {
          console.log(err);
      });
};
fetchtasks = () => {
  const requestBody = {
      query: `
        query {
          tasksList {
            id,
            departmentId,
            companyId,
            teamId
          }
        }
      `
  };

  fetch(baseUrl.server, {
      method: 'POST',
      body: JSON.stringify(requestBody),
      headers: {
          'Content-Type': 'application/json'
      }
  })
      .then(res => {
          if (res.status !== 200 && res.status !== 201) {
              throw new Error('Failed!');
          }
          return res.json();
      })
      .then(resData => {
         
          let tasks = resData.data.tasksList;
          this.setState({ tasks: tasks });
      })

      .catch(err => {
          console.log(err);
      });
};
fetchHabits = () => {
  const requestBody = {
      query: `
        query {
          habitsList {
              habitTitle,
              habitDescription,
              startTime
          }
        }
      `
  };

  fetch(baseUrl.server, {
      method: 'POST',
      body: JSON.stringify(requestBody),
      headers: {
          'Content-Type': 'application/json'
      }
  })
      .then(res => {
          if (res.status !== 200 && res.status !== 201) {
              throw new Error('Failed!');
          }
          return res.json();
      })
      .then(resData => {
          //console.log(resData);
          let habits = resData.data.habitsList;
          this.setState({ habits: habits });
      })
      .catch(err => {
          console.log(err);
      });
};
fetchUsers = () => {
  const requestBody = {
      query: `
        query {
          getAllUsers {
            id,
            firstName,
            lastName,
            emailIs,
            password,
            userRollId,
            companyId,
            departmentId
          }
        }
      `
  };

  fetch(baseUrl.server, {
      method: 'POST',
      body: JSON.stringify(requestBody),
      headers: {
          'Content-Type': 'application/json'
      }
  })
      .then(res => {
          if (res.status !== 200 && res.status !== 201) {
              throw new Error('Failed!');
          }
          return res.json();
      })
      .then(resData => {
          //console.log(resData);
          let users = resData.data.getAllUsers;
          this.setState({ users: users });
      })
      .catch(err => {
          console.log(err);
      });
};

fetchproject = () => {
  const requestBody = {
      query: `
        query {
        getAllProjects {
            projectName,
          }
        }
      `
  };

  fetch(baseUrl.server, {
      method: 'POST',
      body: JSON.stringify(requestBody),
      headers: {
          'Content-Type': 'application/json'
      }
  })
      .then(res => {
          if (res.status !== 200 && res.status !== 201) {
              throw new Error('Failed!');
          }
          return res.json();
      })
      .then(resData => {
          //console.log(resData);
         
          let project = resData.data.getAllProjects;
          this.setState({ projects: project });
      })

      .catch(err => {
          console.log(err);
      });
};

fetchteam = () => {
  const requestBody = {
      query: `
        query {
          getAllTeams {
            id,
            teamName,
          }
        }
      `
  };

  fetch(baseUrl.server, {
      method: 'POST',
      body: JSON.stringify(requestBody),
      headers: {
          'Content-Type': 'application/json'
      }
  })
      .then(res => {
          if (res.status !== 200 && res.status !== 201) {
              throw new Error('Failed!');
          }
          return res.json();
      })
      .then(resData => {
          //console.log(resData);
         
          let team = resData.data.getAllTeams;
          this.setState({ team: team });
      })

      .catch(err => {
          console.log(err);
      });
};
// handledepartmentChange=async(e)=>{
//   e.preventDefault();
//   var data=e.target.value;
  
//   this.setState({ data: e.target.value})


// }
handleChangeCmp=(e)=>{
  this.setState({selectedcmp:e.target.value})
}

handleChangeDept=(e)=>{
  this.setState({selectedDept:e.target.value})
}
handleChangeTm=(e)=>{
  this.setState({selectedTm:e.target.value})
}

handleChangePrj=(e)=>{
  this.setState({selectedPrj:e.target.value})
}


saveCreateCompanyDrawerDetails = async (companyId) => {
  
  this.props.thisObj.setState({
    selectedCompany: this.state.selectedcmp,
    selectedDepartment:this.state.selectedDept,
    selectedProject:this.state.selectedPrj,
    selectedTeam:this.state.selectedTm

  })
  this.closeEditCompanyDrawer()
  //this.props.thisObj.setState({isEditCompDtlsOpen: false})
  };
  closeEditCompanyDrawer=()=>{
   this.props.thisObj.setState({isEditCompDtlsOpen: false})
  }

  toggleDrawer = (side, open) => event => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }

    this.setState({ ...this.state, [side]: open });
  };



  sideList = side => (
    <div>
     {/* {this.state.users.map(user => { */}

      
    <div onClick={this.props.onClick} className={'EdtCmp-drawer-bodySection'} role="presentation">
      <div className='EdtCmp-drawer-heading-section'>
        <p className='filter-drawer-heading'>Edit company details</p>
      </div>

    <FormControl variant="outlined" className='customised-dropdown-menu'>
    <InputLabel htmlFor="name-disabled">Company Name <i class="fa fa-asterisk edtCmp-icon" aria-hidden="true"></i></InputLabel>
          <div className='flex-container'>
       
            
    <img src={companyIcon} alt=''/>
     
    <Select value={this.state.selectedcmp} onClick={(e)=>this.handleChangeCmp(e)} inputProps={{}}>
    { this.state.company != null || this.state.company != undefined ?
      this.state.company.map(company=>{
    return<MenuItem value={company} >{company.companyName}</MenuItem>  
}) : null}
            </Select>
          </div>
        </FormControl> 
          

        <FormControl variant="outlined" className='customised-dropdown-menu' >
          <InputLabel htmlFor="name-disabled">Department <i class="fa fa-asterisk edtCmp-icon" aria-hidden="true"></i></InputLabel>
          <div className='flex-container'>
            <img src={departmentIcon} alt=''/>
            <Select value={this.state.selectedDept} onChange={(e)=>this.handleChangeDept(e)} inputProps={{}}>
            { this.state.department!= null || this.state.department != undefined ?
              this.state.department.map(department1=>{
// if(user.departmentId==department1.id){
              return<MenuItem value={department1}>{department1.departmentName}</MenuItem>
            // }
          }):null}
            </Select>

          </div>
        </FormControl> 

        <FormControl variant="outlined" className='customised-dropdown-menu' >
          <InputLabel htmlFor="name-disabled">Team <i class="fa fa-asterisk edtCmp-icon" aria-hidden="true"></i></InputLabel>
          <div className='flex-container'>
            <img src={teamIcon} alt=''/>
            <Select value={this.state.selectedTm} onChange={(e)=>this.handleChangeTm(e)} inputProps={{}}>
            { this.state.team != null || this.state.team != undefined ?
              this.state.team.map(team=>{
              return<MenuItem value={team}>{team.teamName}</MenuItem>
            }) :null }
            </Select>
          </div>
        </FormControl> 

        <FormControl variant="outlined" className='customised-dropdown-menu' >
          <InputLabel htmlFor="name-disabled">Project <i class="fa fa-asterisk edtCmp-icon" aria-hidden="true"></i></InputLabel>
          <div className='flex-container'>
            <img src={projectIcon} alt=''/>
            <Select value={this.state.selectedPrj}   name='taskProjectid' onChange={e => this.handleChangePrj(e)} inputProps={{}}>
              {this.state.projects != null || this.state.projects != undefined ?
              this.state.projects.map(team=>{
// if(team.projectId==team.id){
  return<MenuItem value={team}>{team.projectName}</MenuItem>
// }
              }) : null }
            </Select>
          </div>
        </FormControl>                 


        <div className='filter-bottom-buttons'>
          <Button variant="contained" className="ch-save-button" onClick={this.closeEditCompanyDrawer}>
            Cancel
          </Button>

          <Button variant="contained" className="ch-submit-button" onClick={this.saveCreateCompanyDrawerDetails}>
            Save
          </Button>
        </div>
       
    </div>
    {/* })} */}
    </div>
  );



  render() {
    return (
      <Drawer anchor="right" open={this.props.isEditCompDtlsOpen}  onClose={this.toggleDrawer('right', false)} className='EdtCmp-drawer'>
        {/* <div className='drawer-closer' onClick={this.closeEditCompanyDrawer}>sssssss</div> */}
        {this.sideList('right')}
      </Drawer>
    )
  }

}

export default FiltersDrawer;
